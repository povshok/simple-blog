<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    use Sluggable;

    protected $fillable = ['title'];

    public function resolveRouteBinding($value)
    {
        return  $this->where('slug', $value)->orWhere('id', $value)->firstOrFail();
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_tags', 'tag_id', 'post_id');
    }
    public function getImage()
    {
        return $this->image ? '/' . $this->image : '';
    }
}

@extends('layout')

@section('top')

@endsection

@section('content')
    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-8 pt-5">
                    @include('admin.errors')
                    <form action="/register" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="name"
                                   placeholder="Your Name"
                                   value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Your Email"
                                   value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control"  name="password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Register" class="btn btn-primary py-3 px-5">
                        </div>
                    </form>
                </div>
                @include('sidebar')
            </div>
        </div>
    </section> <!-- .section -->
@endsection

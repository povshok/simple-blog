<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{env('APP_NAME')}}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand navbar-brand-black" href="/">Explorer</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Home</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="index.html">Home with slider</a>
                        <a class="dropdown-item" href="index2.html">Home with full slider</a>
                        <a class="dropdown-item" href="index3.html">Home with parallax</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Post</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="post-right-sidebar.html">Post with right sidebar</a>
                        <a class="dropdown-item" href="post-left-sidebar.html">Post with left sidebar</a>
                        <a class="dropdown-item" href="post-no-sidebar.html">Post no sidebar</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Archives</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="destination.html">Destination</a>
                        <a class="dropdown-item" href="tag.html">Tag</a>
                        <a class="dropdown-item" href="author-post.html">Authors Post</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Pages</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="right-sidebar.html">Right Sidebar</a>
                        <a class="dropdown-item" href="left-sidebar.html">Left Sidebar</a>
                        <a class="dropdown-item" href="author.html">Authors Page</a>
                    </div>
                </li>
                <li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li>

                @if(Auth::check())
                    <li class="nav-item dropdown pl-5">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown05" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <img class="avatar pr-2" src="{{ Auth::user()->getImage() }}" alt="" height="30">
                            {{Auth::user()->name}}</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown05">
                            @if(Auth::user()->is_admin)
                                <a class="dropdown-item" href="{{ route('dashboard') }}">Admin panel</a>
                            @endif
                            <a class="dropdown-item" href="{{ route('profile.form') }}">Profile</a>
                            <a class="dropdown-item" href="{{route('logout')}}">Logout</a>
                        </div>
                    </li>

                @else
                    <li class="nav-item pl-5"><a href="{{ route('register.form') }}" class="nav-link">Register</a></li>
                    <li class="nav-item"><a href="{{ route('login.form') }}" class="nav-link">Login</a></li>
                @endif

            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->


@yield('top')
@if (session('status'))
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info">
                    {{session('status')}}
                </div>
            </div>
        </div>
    </div>
@endif

@if (request()->is('/'))
    <section class="ftco-intro" data-aos="fade-up" data-aos-duration="3000">
        <div class="container">
            <div class="row justify-content-start">
                <div class="col-md-9">
                    <h1>I'm <strong>Traveler</strong> &amp; <strong>Blogger</strong> from Paris, Italy Who Loves
                        Documenting Adventures &amp; Discoveries Around the World</h1>
                </div>
            </div>
        </div>
    </section>
@endif

@yield('content')

<footer class="ftco-footer ftco-bg-dark ftco-section">
    @include('footer')
</footer>


<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00"/>
    </svg>
</div>


<script src="{{ asset('js/app.js') }}" defer></script>


</body>
</html>

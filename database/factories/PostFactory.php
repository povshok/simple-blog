<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Post;
use Faker\Generator as Faker;


$factory->define(Post::class, function (Faker $faker) {
    $number = range(0,10);
    return [
        'title' => $faker->sentence,
        'description' => $faker->paragraph,
        'content' => '<p>'.$faker->paragraph.'</p><p>'.$faker->text.'</p>',
        'image' => 'uploads/1RbffjFGB5spmaBp6xfZTTJi0cwFmk0jJF7BYUJU.jpeg',
        'date' => $faker->date('d/m/y'),
        'views' => $faker->numberBetween(0, 5000),
        'category_id' => rand(1,5),
        'user_id' => rand(1,10),
        'status' => 1,
        'is_featured' => 0
    ];
});

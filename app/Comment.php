<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Comment extends Model
{

    protected static function boot()
    {
        parent::boot();

//        static::addGlobalScope('allowed', function (Builder $builder) {
//            $builder->where('status', '=', true);
//        });
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function allow()
    {
        $this->status = 1;
        $this->save();
    }

    public function disallow()
    {
        $this->status = 0;
        $this->save();
    }

    public function toggleStatus()
    {
        return $this->status ? $this->disallow() : $this->allow();
    }

    public function scopeAllowed($query)
    {
        return $query->where('status', 1);
    }

    public function scopeDisallowed($query)
    {
        return $query->where('status', 0);
    }

    public function getAllowed()
    {
        return $this->active->get();
    }
}

@extends('layout')

@section('top')

@endsection

@section('content')
    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-8 pt-5">
                    @include('admin.errors')
                    @if(session('status'))
                        <div class="alert alert-success">{{session('status')}}</div>
                    @endif
                    <form action="{{ route('profile') }}" enctype="multipart/form-data" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="name"
                                   placeholder="Your Name"
                                   value="{{ $user->name }}">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Your Email"
                                   value="{{ $user->email }}">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control"  name="password" placeholder="Password">
                        </div>
                        <div class="form-group ">
                            <img class="mb-3" src="{{$user->getImage()}}" width="200" alt="">
                            <div class="custom-file">
                                <input type="file" name="avatar" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">
                                    Аватар
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Update" class="btn btn-primary py-3 px-5">
                        </div>
                    </form>
                </div>
                @include('sidebar')
            </div>
        </div>
    </section> <!-- .section -->
@endsection

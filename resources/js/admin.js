window.Popper = require('popper.js').default;
window.$ = window.jQuery = require('jquery');

require('./bootstrap');
require('startbootstrap-sb-admin-2/js/sb-admin-2.js');
require('datatables.net-bs4');
require('select2');
require('pc-bootstrap4-datetimepicker');

import Vue from "vue";

// require('moment');


$.extend(true, $.fn.datetimepicker.defaults, {
    icons: {
        time: 'far fa-clock',
        date: 'far fa-calendar',
        up: 'fas fa-arrow-up',
        down: 'fas fa-arrow-down',
        previous: 'fas fa-chevron-left',
        next: 'fas fa-chevron-right',
        today: 'fas fa-calendar-check',
        clear: 'far fa-trash-alt',
        close: 'far fa-times-circle'
    },
    format: 'D/MM/YY'
});

import bsCustomFileInput from 'bs-custom-file-input'

$(document).ready(function () {
    bsCustomFileInput.init();
    $('.select2').select2();
    $('#datepicker').datetimepicker();
});


import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

// Or using the CommonJS version:
// const ClassicEditor = require( '@ckeditor/ckeditor5-build-classic' );

let editors = $('textarea');
for (var i = 0; i < editors.length; ++i) {
    ClassicEditor.create(editors[i]);
}

const app = new Vue({
    el: '#wrapper'
});

axios.get('/api/profile').then(res => console.log(res));

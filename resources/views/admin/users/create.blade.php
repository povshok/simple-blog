@extends('admin.layout')

@section('content')
    <form class="user" action="{{route('users.store')}}" enctype="multipart/form-data" method="post">
        @csrf
        <h3>Add user</h3>
        @include('admin.errors')
        <div class="form-row">
            <div class="form-group col-md-6">
                <div class="form-group">
                    <label for="name">Имя</label>
                    <input type="text" name="name" value="{{ old('name') }}" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="text" name="email" value="{{ old('email') }}" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Пароль</label>
                    <input type="password" name="password" class="form-control" id="exampleInputEmail1" placeholder="">
                </div>
                <div class="form-group custom-file">
                    <input type="file" name="avatar" class="custom-file-input" id="exampleInputFile">
                    <label class="custom-file-label" for="exampleInputFile">
                        Аватар
                    </label>
                    <p class="help-block">Какое-нибудь уведомление о форматах..</p>
                </div>
            </div>
        </div>
        <div class="form-row justify-content-between">
            <div class="col-md-4">
                <a href="{{ route('users.index') }}" class="btn btn-secondary btn-user btn-block">Back</a>
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary btn-user btn-block">Submit</button>
            </div>
        </div>
    </form>
@endsection

@extends('admin.layout')

@section('content')
    <form class="user" action="{{route('posts.update', $post)}}" enctype="multipart/form-data" method="post">
        @csrf
        @method('PUT')
        <h3>Add post</h3>
        @include('admin.errors')
        <div class="form-row">
            <div class="form-group col-md-6">
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="title" value="{{ $post->title }}" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <img src="{{ $post->getImage() }}" alt="" width="200" class="mb-2">
                    <div class="custom-file">
                        <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">
                            Лицевая картинка
                        </label>
                        <p class="help-block">Какое-нибудь уведомление о форматах..</p>
                    </div>
                </div>


                <div class="form-group">
                    <label>Категория</label>
                    <select class="form-control select2" name="category_id">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}"
                                {{$post->category_id ===$category->id ? 'selected' :''}} > {{$category->title}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Теги</label>
                    <select class="form-control select2" multiple="multiple" data-placeholder="Выберите теги"
                            name="tags[]">
                        @foreach($tags as $tag)
                            <option value="{{$tag->id}}"
                                {{$post->tags->pluck('id')->contains($tag->id) ? 'selected' :''}}
                            >{{$tag->title}}</option>
                        @endforeach
                    </select>
                </div>
                <!-- Date -->
                <div class="form-group">
                    <label>Дата:</label>

                    <div class="input-group date">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"> <i class="fa fa-calendar"></i></span>
                        </div>
                        <input type="text" class="form-control" name="date" value="{{ $post->date }}" id="datepicker"
                               aria-describedby="basic-addon1">
                    </div>
                    <!-- /.input group -->
                </div>


                <!-- checkbox -->
                <div class="custom-control custom-switch form-group">
                    <input type="checkbox" name="is_featured" class="custom-control-input" id="recommendedSwitch"
                        {{ $post->is_featured ? 'checked' : '' }}>
                    <label class="custom-control-label" for="recommendedSwitch">Recommended</label>
                </div>

                <!-- checkbox -->
                <div class="custom-control custom-switch form-group">
                    <input type="checkbox" name="status" class="custom-control-input" id="draftSwitch"
                        {{ $post->status ? 'checked' : '' }}>
                    <label class="custom-control-label" for="draftSwitch">Draft</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="description">Полный текст</label>
                    <textarea name="description" id="description" cols="30" rows="10"
                              class="form-control">{{ $post->description }}</textarea>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="content">Полный текст</label>
                    <textarea name="content" id="content" cols="30" rows="12"
                              class="form-control">{{ $post->content }}</textarea>
                </div>
            </div>
        </div>
        <div class="form-row justify-content-between">
            <div class="col-md-4">
                <a href="{{ route('posts.index') }}" class="btn btn-secondary btn-user btn-block">Back</a>
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary btn-user btn-block">Submit</button>
            </div>
        </div>
    </form>
@endsection

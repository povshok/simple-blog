<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;

class HomeController extends Controller
{
    public function __invoke()
    {
        $posts = Post::paginate(10);
        $featuredPosts = Post::where('is_featured',1)->take(5)->get();
        return view('pages.index', compact('posts', 'featuredPosts' ));
    }

    public function show(Post $post)
    {
        return view('pages.show', compact('post'));
    }

    public function showByCategory(Category $categoryIdOrSlug)
    {
        $posts = $categoryIdOrSlug->posts()->published()->paginate(10);
        $key   = $categoryIdOrSlug;

        return view('pages.list')->with(compact('posts', 'key'));
    }

    public function showByTag(Tag $tagIdOrSlug)
    {
        $posts = $tagIdOrSlug->posts()->published()->paginate(10);
        $key   = $tagIdOrSlug;

        return view('pages.list')->with(compact('posts', 'key'));
    }
}

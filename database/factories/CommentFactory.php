<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'text'    => $faker->text,
        'user_id' => rand(1, 10),
        'post_id' => rand(1, 10),
        'status'  => 1,
        'created_at' => $faker->dateTimeThisYear(),
    ];
});

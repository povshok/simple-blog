<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    use Sluggable;

    protected $fillable = ['title'];

    public function resolveRouteBinding($value)
    {
        return  $this->where('slug', $value)->orWhere('id', $value)->firstOrFail();
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function getImage()
    {
        return $this->image ? '/' . $this->image : '';
    }
}

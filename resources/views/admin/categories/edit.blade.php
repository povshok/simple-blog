@extends('admin.layout')

@section('content')
    <form class="user" action="{{route('categories.update', $category->id)}}" method="post">
        @csrf
        @method('PUT')
        <h3>Update category</h3>
        @include('admin.errors')
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="CategoryName">Name</label>
                <input type="text" class="form-control" id="CategoryName" name="title" value="{{$category->title}}">
            </div>
        </div>
        <div class="form-row justify-content-between">
            <div class="col-md-4">
                <a href="{{route('categories.index')}}" class="btn btn-secondary btn-user btn-block">Back</a>
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary btn-user btn-block">Submit</button>
            </div>
        </div>
    </form>
@endsection

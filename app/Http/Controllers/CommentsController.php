<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);

        $comment = new Comment;
        $comment->text = $request->message;
        $comment->post_id = $request->post_id;
        $comment->user_id = Auth::user()->id;
        $comment->save();

        return redirect()->back()->with('status', 'Comment saved');
    }
}

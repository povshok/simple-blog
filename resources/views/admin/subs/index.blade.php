@extends('admin.layout')

@section('content')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h3 class="card-title">Simple Full Width Table</h3>
            <a href="{{route('subscribers.create')}}" class="btn btn-success">Add subscriber</a>

        </div>

        <div class="card-body">
            <div class="table-responsive overflow-hidden">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th style="width: 10px">ID</th>
                        <th>email</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th style="width: 10px">ID</th>
                        <th>email</th>
                        <th>Actions</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($subscribers as $subscriber)
                        <tr>
                            <td>{{$subscriber->id}}</td>
                            <td>{{$subscriber->email}}</td>
                            <td>
                                <form action="{{route('subscribers.destroy', $subscriber->id)}}" class="d-inline-block"
                                      method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button onclick="return confirm('are you sure?')" type="submit"
                                            class="btn btn-outline-light px-2">
                                        <i class="fas fa-2x fa-trash-alt text-decoration-none text-danger"></i>
                                    </button>
                                </form>

                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

@section('scripts')



    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });

    </script>


@endsection

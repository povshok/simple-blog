<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController');
Route::get('post/{post}', 'Homecontroller@show')->name('post.show');
Route::get('tag/{tagIdOrSlug}', 'Homecontroller@showByTag')->name('tag.show');
Route::get('category/{categoryIdOrSlug}', 'Homecontroller@showByCategory')->name('category.show');
Route::post('subscribe', 'SubsController@subscribe')->name('subscribe');
Route::get('verify/{token}', 'SubsController@verify');

Route::group(['middleware' => 'auth'], function () {
    Route::get('profile', 'ProfileController@index')->name('profile.form');
    Route::post('profile', 'ProfileController@store')->name('profile');
    Route::get('logout', 'AuthController@logout')->name('logout');
    Route::post('comment', 'CommentsController')->name('comment');
});

Route::group(['middleware' => 'guest'], function () {
    Route::get('login', 'AuthController@loginForm')->name('login.form');
    Route::post('login', 'AuthController@login')->name('login');
    Route::get('register', 'AuthController@registerForm')->name('register.form');
    Route::post('register', 'AuthController@register')->name('register');
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {
    Route::get('/', 'DashboardController')->name('dashboard');
    Route::resource('categories', 'CategoriesController');
    Route::resource('tags', 'TagsController');
    Route::resource('users', 'UsersController');
    Route::resource('posts', 'PostsController');
    Route::get('comments', 'CommentsController@index')->name('comments.index');
    Route::get('comments/toggle/{comment}', 'CommentsController@toggle')->name('comments.toggle');
    Route::delete('comments/{comment}/destroy', 'CommentsController@destroy')->name('comments.destroy');
    Route::resource('subscribers', 'SubscribersController');
});

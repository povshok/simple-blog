<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Main Navigation
    </div>

    <!-- Nav Item - Posts -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('posts.index')}}">
            <i class="fas fa-file"></i>
            <span>Posts</span></a>
    </li>

    <!-- Nav Item - Categories -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('categories.index')}}">
            <i class="fas fa-list-ul"></i>
            <span>Categories</span></a>
    </li>

    <!-- Nav Item - Tags -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('tags.index')}}">
            <i class="fas fa-tags"></i>
            <span>Tags</span></a>
    </li>

    <!-- Nav Item - Users -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('users.index')}}">
            <i class="fas fa-users"></i>
            <span>Users</span></a>
    </li>

    <!-- Nav Item - Comments -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('comments.index')}}">
            <i class="fas fa-comment"></i>
            <span>Comments</span></a>
    </li>

    <!-- Nav Item - Subscribers -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('subscribers.index')}}">
            <i class="fas fa-clipboard-list"></i>
            <span>Subscribers</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>

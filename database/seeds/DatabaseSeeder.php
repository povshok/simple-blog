<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 10)->create();
        factory(\App\Category::class, 5)->create();
        factory(\App\Tag::class, 10)->create();
        $posts = factory(\App\Post::class, 100)->create();
        $posts->each(function ($post) {
            $number = range(0, 10);
            $post->setTags(array_rand($number, rand(1, 10)));
            $post->toggleFeatured(mt_rand(0, 1));
            factory(\App\Comment::class, rand(1,10))->create(['post_id'=> $post->id]);
        });
    }
}

@extends('layout')

@section('top')
<div class="my-5"></div>
@endsection

@section('content')
    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-8 pt-5">
                    @include('admin.errors')
                    @if(session('status'))
                        <p>{{session('status')}}</p>
                    @endif
                    <form action="{{route('login')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Your Email"
                                   value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control"  name="password" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Login" class="btn btn-primary py-3 px-5">
                        </div>
                    </form>
                </div>
                @include('sidebar')
            </div>
        </div>
    </section> <!-- .section -->
@endsection

@extends('layout')

@section('top')
    <div class="hero-wrap js-fullheight" style="background-image: url({{ $post->getImage() }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center"
                 data-scrollax-parent="true">
                <div class="col-md-9 text-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                    <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span
                            class="mr-2"><a href="index.html">Home</a></span> <span>Articles</span></p>
                    <h1 class="mb-3 bread"
                        data-scrollax="properties: { translateY: '80%', opacity: 1.6 }">{{$post->title}}</h1>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate">
                    {!! $post->content !!}
                    <div class="tag-widget post-tag-container mb-5 mt-5">
                        <div class="tagcloud">
                            <a href="#" class="tag-cloud-link">Life</a>
                            <a href="#" class="tag-cloud-link">Sport</a>
                            <a href="#" class="tag-cloud-link">Tech</a>
                            <a href="#" class="tag-cloud-link">Travel</a>
                        </div>
                    </div>

                    <div class="about-author d-flex p-4 bg-light">
                        <div class="bio mr-5">
                            <img src="{{ $post->author->getImage() }}" alt="Image placeholder"
                                 class="img-fluid mb-4 avatar">
                        </div>
                        <div class="desc">
                            <h3>{{ $post->author->name }}</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem
                                necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa
                                sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
                        </div>
                    </div>


                    <div class="pt-5 mt-5">
                        <h3 class="mb-5">{{ $post->comments->count() }} Comments</h3>
                        @if($post->comments()->count())
                            <ul class="comment-list">
                                @foreach($post->comments  as $comment)
                                    <li class="comment">
                                        <div class="vcard bio">
                                            <img src="{{ $comment->author->getImage() }}" alt="Image placeholder">
                                        </div>
                                        <div class="comment-body">
                                            <h3>{{ $comment->author->name }}</h3>
                                            <div class="meta">{{ $comment->created_at->diffForHumans() }}</div>
                                            <p>{{ $comment->text }}</p>
{{--                                            <p><a href="#" class="reply">Reply</a></p>--}}
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <!-- END comment-list -->
                        @endif
                        @if(Auth::check())
                            <div class="comment-form-wrap pt-5">
                                <h3 class="mb-5">Leave a comment</h3>
                                <form action="{{ route('comment') }}" class="p-5 bg-light" method="post">
                                    @csrf
                                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                                    <div class="form-group">
                                        <label for="message">Comment</label>
                                        <textarea name="message" id="message" cols="30" rows="10"
                                                  class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="Post Comment" class="btn py-3 px-4 btn-primary">
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>

                </div> <!-- .col-md-8 -->
                @include('sidebar')

            </div>
        </div>
    </section> <!-- .section -->
@endsection

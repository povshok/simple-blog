<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public static function add($fields)
    {
        $user = new static;
        $user->fill($fields);
        $user->save();

        return $user;
    }

    public function edit($fields)
    {
        $this->fill($fields);

        $this->save();
    }

    public function generatePassword($password)
    {
        if($password != null) {
            $this->password = bcrypt($password);
            $this->save();
        }
    }

    public function remove()
    {
        Storage::delete( $this->avatar);
        $this->delete();
    }

    public function uploadAvatar($image)
    {
        if ($image == null) {
            return;
        } else {
            Storage::delete( $this->avatar);
        }

        $path = Storage::putFile('uploads', $image);

        $this->avatar = $path;
        $this->save();
    }

    public function getImage()
    {
        return $this->avatar ? '/'. $this->avatar : '/img/no-image.png';
    }

    public function setNormal()
    {
        $this->is_admin = 0;
        $this->save();
    }

    public function setAdmin()
    {
        $this->is_admin = 1;
        $this->save();
    }

    public function toggleAdmin($value)
    {
        return $value ? $this->setAdmin() : $this->setNormal();
    }

    public function unban()
    {
        $this->status = 0;
        $this->save();
    }

    public function ban()
    {
        $this->status = 1;
        $this->save();
    }

    public function toggleBan($value)
    {
        return $value ? $this->ban() : $this->unban();
    }
}

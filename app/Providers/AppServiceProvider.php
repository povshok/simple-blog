<?php

namespace App\Providers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('sidebar', function ($view){
            $view->with('popularPosts', Post::orderBy('views', 'desc')->take(3)->get());
            $view->with('categories', Category::all());
            $view->with('tags', Tag::all());

        });
        view()->composer('footer', function ($view){
            $view->with('categories', Category::all());
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

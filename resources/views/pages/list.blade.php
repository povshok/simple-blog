@extends('layout')

@section('top')
    <div class="hero-wrap js-fullheight" style="background-image: url({{ $key->getImage() }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
                <div class="col-md-9 text-center ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
                    <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="index.html">Home</a></span> <span>Articles</span></p>
                    <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '80%', opacity: 1.6 }">{{$key->title}}</h1>
                    <span class="d-block mb-4">{{$key->posts()->count()}} Articles</span>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-md-12">
                                <div class="blog-entry ftco-animate">
                                    <a href="{{ route('post.show', $post->slug) }}" class="img" style="background-image: url({{$post->getImage()}});"></a>
                                    <div class="text pt-2 mt-5">
                                        <span class="big">{{$post->category->title}}</span>
                                        <h3 class="mb-4"><a href="#">{{ $post->title }}</a></h3>
                                        <div class="mb-4">{!! $post->description !!}</div>
                                        <div class="author mb-4 d-flex align-items-center">
                                            <a href="#" class="img"
                                               style="background-image: url(images/person_1.jpg);"></a>
                                            <div class="ml-3 info">
                                                <span>Written by</span>
                                                <h3><a href="#">{{ $post->author->name }}</a>, <span>{{ $post->getDate() }}</span></h3>
                                            </div>
                                        </div>
                                        <div class="meta-wrap d-md-flex align-items-center">
                                            <div class="half order-md-last text-md-right">
                                                <p class="meta">
                                                    <span><i class="icon-heart"></i>3</span>
                                                    <span><i class="icon-eye"></i>{{ $post->views }}</span>
                                                    <span><i class="icon-comment"></i>5</span>
                                                </p>
                                            </div>
                                            <div class="half">
                                                <p><a href="{{ route('post.show', $post->slug) }}" class="btn btn-primary p-3 px-xl-4 py-xl-3">Continue
                                                        Reading</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    {{$posts->links('pagination')}}
                </div><!-- END-->
                @include('sidebar')
            </div>
        </div>
    </section>
@endsection

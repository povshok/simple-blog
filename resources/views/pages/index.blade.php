@extends('layout')
@section('top')
    @include('carousel');
@endsection
@section('content')
    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        @foreach($posts as $post)
                            <div class="col-md-12">
                                <div class="blog-entry" data-aos="fade-up">
                                    <a href="{{ route('post.show', $post->slug) }}" class="img" style="background-image: url({{$post->getImage()}});"></a>
                                    <div class="text pt-2 mt-5">
                                        <span class="big">{{$post->category->title}}</span>
                                        <h3 class="mb-4"><a href="#">{{ $post->title }}</a></h3>
                                        <div class="mb-4">{!! $post->description !!}</div>
                                        <div class="author mb-4 d-flex align-items-center">
                                            <a href="#" class="img"
                                               style="background-image: url({{ $post->author->getImage() }});"></a>
                                            <div class="ml-3 info">
                                                <span>Written by</span>
                                                <h3><a href="#">{{ $post->author->name }}</a>, <span>{{ $post->getDate() }}</span></h3>
                                            </div>
                                        </div>
                                        <div class="meta-wrap d-md-flex align-items-center">
                                            <div class="half order-md-last text-md-right">
                                                <p class="meta">
                                                    <span><i class="icon-heart"></i>3</span>
                                                    <span><i class="icon-eye"></i>{{ $post->views }}</span>
                                                    <span><i class="icon-comment"></i>{{ $post->comments->count() }}</span>
                                                </p>
                                            </div>
                                            <div class="half">
                                                <p><a href="{{ route('post.show', $post->slug) }}" class="btn btn-primary p-3 px-xl-4 py-xl-3">Continue
                                                        Reading</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    {{$posts->links('pagination')}}
                </div><!-- END-->
               @include('sidebar')
            </div>
        </div>
    </section>
@endsection

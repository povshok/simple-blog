<div class="col-lg-4 sidebar ftco-animate " data-aos="fade-up">
    <div class="sidebar-box">
        <form action="#" class="search-form">
            <div class="form-group">
                <span class="icon icon-search"></span>
                <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
            </div>
        </form>
    </div>
    <div class="sidebar-box" data-aos="fade-left">
        <h3>Categories</h3>
        <ul class="categories">
            @foreach($categories as $category)
                <li>
                    <a href="{{ route('category.show', $category->slug) }}">
                        {{$category->title}}
                        <span>({{ $category->posts()->count() }})</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="sidebar-box ftco-animate" data-aos="fade-left">
        <h3>Popular Articles</h3>
        @foreach($popularPosts as $post)
            <div class="block-21 mb-4 d-flex">
                <a class="blog-img mr-4" style="background-image: url({{ $post->getImage() }});"></a>
                <div class="text">
                    <h3 class="heading"><a href="{{ route('post.show', $post->slug) }}">{{ $post->title }}</a></h3>
                    <div class="meta">
                        <div><a href="#"><span class="icon-calendar"></span> {{ $post->getDate() }}</a></div>
                        <div><a href="#"><span class="icon-person"></span> {{ $post->author->name }}</a></div>
                        <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="sidebar-box" data-aos="fade-left">
        <h3>Tag Cloud</h3>
        <ul class="tagcloud">
            @foreach($tags as $tag)
            <a href="{{ route('tag.show', $tag->slug) }}" class="tag-cloud-link">{{ $tag->title }}</a>

            @endforeach
        </ul>
        </ul>
    </div>

    <div class="sidebar-box subs-wrap" data-aos="flip-up">
        <h3>Subcribe to our Newsletter</h3>
        <p>Far far away, behind the word mountains, far from the countries Vokalia</p>
        <form action="{{ route('subscribe') }}" class="subscribe-form" method="post">
            @csrf
            <div class="form-group">
                <input type="text" name="email" class="form-control" placeholder="Email Address">
                <input type="submit" value="Subscribe" class="mt-2 btn btn-white submit">
            </div>
        </form>
    </div>

    <div class="sidebar-box" data-aos="fade-left">
        <h3>Archives</h3>
        <ul class="categories">
            <li><a href="#">September 2018 <span>(6)</span></a></li>
            <li><a href="#">August 2018 <span>(8)</span></a></li>
            <li><a href="#">July 2018 <span>(2)</span></a></li>
            <li><a href="#">June 2018 <span>(7)</span></a></li>
            <li><a href="#">May 2018 <span>(5)</span></a></li>
            <li><a href="#">April 2018 <span>(3)</span></a></li>
        </ul>
    </div>


    <div class="sidebar-box" data-aos="fade-left">
        <h3>Paragraph</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem
            necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente
            consectetur similique, inventore eos fugit cupiditate numquam!</p>
    </div>
</div><!-- END COL -->

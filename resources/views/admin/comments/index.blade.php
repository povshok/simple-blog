@extends('admin.layout')

@section('content')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h3 class="card-title">Simple Full Width Table</h3>
            <a href="{{route('users.create')}}" class="btn btn-success">Add user</a>

        </div>

        <div class="card-body">
            <div class="table-responsive overflow-hidden">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th style="width: 10px">ID</th>
                        <th>Text</th>
                        <th>Post</th>
                        <th>Author</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th style="width: 10px">ID</th>
                        <th>Text</th>
                        <th>Post</th>
                        <th>Author</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($comments as $comment)
                        <tr>
                            <td>{{$comment->id}}</td>
                            <td>{{$comment->text}}</td>
                            <td>{{$comment->post_id}}</td>
                            <td>{{$comment->author->name}}</td>
                            <td>{{$comment->status}}</td>
                            <td class="d-flex align-items-stretch">
                                @if($comment->status)
                                    <a href="{{ route('comments.toggle', $comment) }}"
                                       class="btn btn-outline-light px-2">
                                        <i class="fas  fa-times text-decoration-none text-warning"></i></a>
                                @else
                                    <a href="{{ route('comments.toggle', $comment) }}"
                                       class="btn btn-outline-light px-2">
                                        <i class="fas  fa-check text-decoration-none text-success"></i></a>
                                @endif

                                <form action="{{ route('comments.destroy', $comment) }}" class="d-inline-block"
                                      method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button onclick="return confirm('are you sure?')" type="submit"
                                            class="btn btn-outline-light px-2">
                                        <i class="fas fa-trash-alt text-decoration-none text-danger"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

@section('scripts')



    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });

    </script>


@endsection

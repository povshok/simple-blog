@if($featuredPosts->count())
    <section class="home-slider js-fullheight owl-carousel">
        @foreach($featuredPosts as $post)
            <div class="slider-item js-fullheight">
                <div class="container-fluid">
                    <div class="row js-fullheight slider-text justify-content-center align-items-center"
                         data-scrollax-parent="true">

                        <div class="col-md-6 text ftco-animate">
                            <div class="author mb-4 d-flex align-items-center">
                                <a href="#" class="img" style="background-image: url({{ $post->getImage() }});"></a>
                                <div class="ml-3 info">
                                    <span>Written by</span>
                                    <h3><a href="#">{{ $post->author->name }}</a>, <span>{{ $post->getDate() }}</span>
                                    </h3>
                                </div>
                            </div>
                            <div class="text-2">
                                <span class="big">{{$post->getCategoryTitle()}}</span>
                                <h1 class="mb-4"><a href="{{ route('post.show', $post->slug) }}">{{ $post->title }}</a>
                                </h1>
                                <div class="mb-4">{!! $post->description !!}</div>
                                <p><a href="{{ route('post.show', $post->slug) }}"
                                      class="btn btn-primary p-3 px-xl-4 py-xl-3">Continue Reading</a></p>
                            </div>
                        </div>

                        <div class="col-md-6 js-fullheight img"
                             style="background-image: url({{ $post->getImage() }}    );"></div>

                    </div>
                </div>
            </div>
        @endforeach
    </section>
@endif

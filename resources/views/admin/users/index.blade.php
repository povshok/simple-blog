@extends('admin.layout')

@section('content')

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h3 class="card-title">Simple Full Width Table</h3>
            <a href="{{route('users.create')}}" class="btn btn-success">Add user</a>

        </div>

        <div class="card-body">
            <div class="table-responsive overflow-hidden">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th style="width: 10px">ID</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Avatar</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th style="width: 10px">ID</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <th>Avatar</th>
                        <th>Actions</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td><img src="{{$user->getImage()}}" alt="{{$user->name}}" class="img-thumbnail" width="100px"></td>
                            <td>
                                <a href="{{route('users.edit', $user)}}"
                                   class="btn btn-outline-light px-2">
                                    <i class="fas fa-2x fa-pencil-alt text-decoration-none text-success"></i></a>
                                <form action="{{route('users.destroy', $user)}}" class="d-inline-block"
                                      method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button onclick="return confirm('are you sure?')" type="submit"
                                            class="btn btn-outline-light px-2">
                                        <i class="fas fa-2x fa-trash-alt text-decoration-none text-danger"></i>
                                    </button>
                                </form>

                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection

@section('scripts')



    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });

    </script>


@endsection

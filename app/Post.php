<?php

namespace App;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    use Sluggable;

    protected $fillable = ['title', 'content', 'date', 'description'];

//    protected $dates = ['date'];

    public function resolveRouteBinding($value)
    {
        return  $this->where('slug', $value)->orWhere('id', $value)->firstOrFail();
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tags', 'post_id', 'tag_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::createFromFormat('d/m/y', $value)->toDateString();

    }

    public function getDateAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/y');
    }

    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }

    public static function add($fields)
    {
        $post = new self;
        $post->fill($fields);
        $post->user_id = 1;
        $post->save();

        return $post;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    public function remove()
    {
        Storage::delete($this->image);
        $this->delete();
    }

    public function uploadImage($image)
    {
        if ($image == null) {
            return;
        } else {
            Storage::delete($this->image);
        }

        $path = Storage::putFile('uploads', $image);

        $this->image = $path;
        $this->save();
    }

    public function getImage()
    {
        return $this->image ? '/' . $this->image : '/img/no-image.png';
    }

    public function setCategory(int $id)
    {
        if ($id === null)
            return;

        $this->category_id = $id;
        $this->save();
    }

    public function setTags($ids = [])
    {
        if ($ids === null)
            return;

        $this->tags()->sync($ids);
        $this->save();
    }

    public function setDraft()
    {
        $this->status = 0;
        $this->save();
    }

    public function setPublic()
    {
        $this->status = 1;
        $this->save();
    }

    public function toggleStatus($value)
    {
        return $value === null ? $this->setDraft() : $this->setPublic();
    }

    public function setFeatured()
    {
        $this->is_featured = 1;
        $this->save();
    }

    public function setStandart()
    {
        $this->is_featured = 0;
        $this->save();
    }

    public function toggleFeatured($value)
    {
        return $value == null ? $this->setStandart() : $this->setFeatured();
    }

    public function getCategoryTitle()
    {
        return $this->category->title ?? 'No category';
    }

    public function getTagsTitles()
    {
        return !$this->tags->isEmpty() ? implode(', ', $this->tags->pluck('title')->all()) : 'No tags';
    }

    public function getDate()
    {
        return Carbon::createFromFormat('d/m/y', $this->date)->format('F d, Y');
    }

}
